<?php

class Converter
{
    private $parts = [
        'honderd' => 100,
        'duizend' => 1000,
        'miljoen' => 1000000,
        'miljard' => 1000000000,
        'nul' => 0,
        'twee' => 2,
        'drie' => 3,
        'vier' => 4,
        'vijf' => 5,
        'zes' => 6,
        'zeven' => 7,
        'tach' => 8,
        'acht' => 8,
        'negen' => 9,
        'tien' => 'TEN', //special case: vijftien means 15, not 50 (while vijfhonderd is indeed 500, not 105)
        'elf' => 11,
        'twaalf' => 12,
        'der' => 3,
        'veer' => 4,
        'twin' => 2,
        'een' => 1,
        'en' => null, //"en" as in drie"en"twintig is unneeded, we can cut it out and the number is still processable
        'tig' => 10,
    ];

    public function transform($literal)
    {
        //sanitize input (lower case, no spaces)
        $literal = strtolower($literal);
        $literal = str_replace(' ', '', $literal);

        //replace all literal parts by its numeric counterparts followed by '|' as a separator
        //eg: 'vijfhonderddertien' => '5|100|3|10'
        foreach($this->parts as $part => $replacement) {
            $literal = str_replace($part, $replacement.'|', $literal);
        }

        //split to array, and remove empty values
        //eg: [5,100,3,10]
        $segments = array_values(array_filter(explode('|', $literal), function($val) {
            return $val !== '' && $val !== null;
        }));

        $segments = $this->processTen($segments);
        $segments = $this->processSmallBeforeBig($segments, 1);
        $segments = $this->processMultiplesOfTen($segments);

        foreach(range(2,20) as $exponent) {
            $segments = $this->processSmallBeforeBig($segments, $exponent);
            $segments = $this->processBigBeforeSmall($segments, $exponent);
        }

        //if all is well, there is only one element left in the array, which contains the processed number
        return $segments[0];
        //return implode(',', $segments);
    }


    /*
     * processes the special case of "ten" (vijftien => 15 although vijfhonderd => 500)
     * eg: [.., 5, 10, ..] ==> [.., 15, ..]
     */
    private function processTen($segments)
    {
        $result = [];
        foreach($segments as $index => $segment) {
            if($index > 0 && $segment === 'TEN' && $segments[$index-1] < 10) {
                array_pop($result);
                $result[] = 10+$segments[$index-1];
            } else {
                $result[] = $segment;
            }
        }
        return $result;
    }


    /*
     * processes numbers followed by a multiple of 10 by adding them to the multiple
     * eg: [..,5,30,..] => [..,35,..]
     */
    private function processMultiplesOfTen($segments)
    {
        $matches = [20,30,40,50,60,70,80,90];
        $result = [];
        foreach($segments as $i => $segment) {
            if($i > 0 && in_array($segment, $matches) && $segments[$i-1] < 10) {
                array_pop($result);
                $result[] = $segment+$segments[$i-1];
            } else {
                $result[] = $segment;
            }
        }
        return $result;
    }


    /*
     * multiplies any number followed by a multiple of ten, as long as it is smaller than the multiple of ten
     * eg: [.., 312, 1000, ..] => [.., 312000, ..]
     */
    private function processSmallBeforeBig($segments, $exponent)
    {
        $powerOfTen = pow(10, $exponent);

        $result = [];
        foreach($segments as $i => $segment) {
            if($i > 0 && $segment == $powerOfTen && $segments[$i-1] < $powerOfTen) {
                array_pop($result);
                $result[] = $segments[$i-1] * $powerOfTen;
            } else {
                $result[] = $segment;
            }
        }
        return $result;
    }


    /*
     * adds any number preceded by a power of 10, or a multiple of a power of ten, to this number
     * eg: [.., 200, 5, ..] => [.., 205, ..]
     */
    private function processBigBeforeSmall($segments, $exponent)
    {
        $powerOfTen = pow(10, $exponent);

        $result = [];
        foreach($segments as $i => $segment) {
            if($i > 0 && $segments[$i-1] / $powerOfTen < 10 && $segments[$i-1] >= $powerOfTen && $segment < $powerOfTen) {
                array_pop($result);
                $result[] = $segments[$i-1] + $segment;
            } else {
                $result[] = $segment;
            }
        }
        return $result;
    }
}

