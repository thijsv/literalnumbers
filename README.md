# literalnumbers

Basic implementation of a string-to-number converter for Dutch numbers. Written in 2 hours, for a time-limited coding challenge. (do not use in production)

To run the tests: `php test.php`  