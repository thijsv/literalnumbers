<?php

include 'Converter.php';

if($argc < 2) die("missing parameter");

echo (new Converter)->transform( $argv[1] );
