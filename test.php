<?php

include 'Converter.php';

$transformer = new Converter;

$cases = [
    'nul' => 0,
    'een' => 1,
    'vijf' => 5,
    'twaalf' => 12,
    'zeventien' => 17,
    'tachtig' => 80,
    'vijfentachtig'=> 85,
    'honderdvijf'=> 105,
    'honderdvijftien'=> 115,
    'duizendeen' => 1001,
    'dertienhonderd' => 1300,
    'duizenddriehonderd' => 1300,
    'drieentwintighonderd' => 2300,
    'twintigduizend' => 20000,
    'twintigduizendvijfhonderdtwaalf' => 20512,
    'drieenveertigduizendvijfhonderddrie' => 43503,
    'honderdduizend' => 100000,
    'honderdduizendtwaalf' => 100012,
    'Honderdvierentwintigduizend driehonderdvijftig' => 124350,
    'miljoenduizend' => 1001000,
    'driemiljoenduizend' => 3001000,
    'driemiljoenvijfhonderdduizenddrieentwintig' => 3500023,
    'tweeduizendmiljoenvijf' => 2000000005,
    'tweemiljardvijf' => 2000000005,
    'honderdvijftienmiljardtweemiljoenvijfhonderdduizenddrieentwintig' => 115002500023,
];

$format = "%80s %30s %20s %10s \n";

printf($format, 'input', 'expected', 'result', 'pass?');

foreach($cases as $input => $expected) {
    $output = $transformer->transform($input);
    printf($format, $input, $cases[$input], $output, $output == $expected ? 'yes' : 'no');
}


